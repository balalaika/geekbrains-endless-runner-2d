﻿using System;
using UnityEngine;

namespace Geekbrains_endless_runner_sample
{
    public class Coin : MonoBehaviour
    {
        [SerializeField] private string _characterTag = "Player";
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (_characterTag != other.gameObject.tag) return;

            var characterScript = other.gameObject.GetComponent<Character>();
            
            characterScript.CoinCollector.Collect();
            
            Destroy(gameObject);
        }
    }
}