﻿namespace Geekbrains_endless_runner_sample
{
    public interface IMoveable : IUpdateHandler
    {
        void Run();
        void Jump();
    }
}