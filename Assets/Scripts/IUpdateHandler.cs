﻿using System;

namespace Geekbrains_endless_runner_sample
{
    public interface IUpdateHandler
    {
        Action GetOnUpdateAction();
    }
}