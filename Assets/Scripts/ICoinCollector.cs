﻿
using System;

namespace Geekbrains_endless_runner_sample
{
    public interface ICoinCollector
    {
        bool IsCoin(string objectTag);
        void Collect();
        int GetCountOfCoins();
    }
}