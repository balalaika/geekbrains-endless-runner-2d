﻿namespace Geekbrains_endless_runner_sample
{
    public enum RunnerMoveState : byte
    {
        Idle = 0,
        Run
    }
}