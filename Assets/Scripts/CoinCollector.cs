﻿using System;
using UnityEngine;

namespace Geekbrains_endless_runner_sample
{
    public class CoinCollector : ICoinCollector
    {
        private string _tag;

        private int _coinCount = 0;

        public Action<int> OnCollect;

        public CoinCollector(Action<int> onCollect)
        {
            OnCollect = onCollect;
        }

        public bool IsCoin(string objectTag)
        {
            return _tag == objectTag;
        }

        public void Collect()
        {
            _coinCount++;

            OnCollect?.Invoke(_coinCount);
        }

        public int GetCountOfCoins()
        {
            return _coinCount;
        }
    }
}