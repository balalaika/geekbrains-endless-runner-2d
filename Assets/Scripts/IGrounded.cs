﻿namespace Geekbrains_endless_runner_sample
{
    public interface IGrounded
    {
        bool IsOnGround();
    }
}