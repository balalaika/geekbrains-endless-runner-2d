﻿using System;
using TMPro;
using UnityEngine;

namespace Geekbrains_endless_runner_sample
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Character : MonoBehaviour, IMoveable
    {
        [Header("References")] 
        [SerializeField] private Grounded _grounded;
        [SerializeField] private TextMeshPro _textMesh;
        
        [Header("Move settings")]
        [SerializeField] private float _runVelocity = 10;
        [SerializeField] private float _jumpForce = 100;
        
        private Rigidbody2D _body;
        
        private IMoveable _moveable;
        
        private ICoinCollector _coinCollector;
        public ICoinCollector CoinCollector => _coinCollector;

        private void Awake()
        {
            _body = GetComponent<Rigidbody2D>();
            
            _moveable = new RunnerMoveable(_body, _grounded, _runVelocity, _jumpForce);
            _coinCollector = new CoinCollector(
                (cointCount) =>
                {
                    _textMesh.text = cointCount.ToString();
                }
            );
        }

        private void Update()
        {
            _moveable?.GetOnUpdateAction()?.Invoke();
        }

        public Action GetOnUpdateAction()
        {
            throw new NotImplementedException();
        }

        public void Run()
        {
            _moveable.Run();
        }

        public void Jump()
        {
            _moveable.Jump();
        }
    }
}