﻿using System;
using UnityEngine;

namespace Geekbrains_endless_runner_sample
{
    public class RunnerMoveable : IMoveable
    {
        private readonly Vector2 _runVelocity;
        private readonly Vector2 _jumpForce;
        
        private Rigidbody2D _body;
        private IGrounded _grounded;

        private RunnerMoveState _state = RunnerMoveState.Idle;
        
        private Action _onUpdate;
        
        public RunnerMoveable(Rigidbody2D body, IGrounded grounded, float runForce, float jumpForce)
        {
            _body = body;
            _grounded = grounded;
            
            _runVelocity = new Vector2(runForce, 0);
            _jumpForce = new Vector2(0, jumpForce);

            _onUpdate += Run;
        }
        
        public void Run()
        {
            if (_state == RunnerMoveState.Idle) return;
            
            _body.velocity = new Vector2(_runVelocity.x, _body.velocity.y);
        }

        public void Jump()
        {
            if (!_grounded.IsOnGround()) return;

            if (_state == RunnerMoveState.Idle)
            {
                _state = RunnerMoveState.Run;
                return;
            }
            
            _body.AddForce(_jumpForce);
        }

        public Action GetOnUpdateAction()
        {
            return _onUpdate;
        }
    }
}