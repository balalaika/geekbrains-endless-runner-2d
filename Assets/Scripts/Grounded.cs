﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Geekbrains_endless_runner_sample
{
    public class Grounded : MonoBehaviour, IGrounded
    {
        [SerializeField] private string _groundTag;
        
        public UnityEvent OnEnter;
        public UnityEvent OnExit;

        private bool _isGrounded = false;
        public bool IsGrounded => _isGrounded;

        private void SetOnGround(string tag)
        {
            if (_isGrounded) return;

            if (_groundTag == tag)
            {
                _isGrounded = true;
                OnEnter?.Invoke();
            }
        }

        private void SetNotOnGround(string tag)
        {
            if (!_isGrounded) return;
            
            if (_groundTag == tag)
            {
                _isGrounded = false;
                OnExit?.Invoke();
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            SetOnGround(other.tag);
        }

        private void OnTriggerExit2D(Collider2D other) 
        {
            SetNotOnGround(other.tag);
        }

        public bool IsOnGround()
        {
            return _isGrounded;
        }
    }
}