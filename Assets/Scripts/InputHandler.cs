﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Geekbrains_endless_runner_sample
{
    public class InputHandler : MonoBehaviour
    {
        [SerializeField] private string _jumpAxisName = "Jump";

        public UnityEvent OnJumpHandle;

        void Update()
        {
            if (Input.GetButtonDown(_jumpAxisName))
            {
                OnJumpHandle?.Invoke();
            }
        }
    }
}